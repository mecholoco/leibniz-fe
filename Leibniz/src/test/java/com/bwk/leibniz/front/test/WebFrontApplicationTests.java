package com.bwk.leibniz.front.test;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwk.leibniz.WebFrontApplication;
import com.bwk.leibniz.front.entity.web.Course;
import com.bwk.leibniz.front.entity.web.Quiz;
import com.bwk.leibniz.front.entity.web.Quizzes;
import com.bwk.leibniz.front.entity.web.Student;
import com.bwk.leibniz.front.service.web.interfaces.CourseServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.QuizServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.StudentServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebFrontApplication.class)
public class WebFrontApplicationTests {

	@Autowired
	@Qualifier("CourseService")
	private CourseServiceInterface cService = null;
	
	@Autowired
	@Qualifier("StudentService")
	private StudentServiceInterface sService = null;
	
	@Autowired
	@Qualifier("QuizService")
	private QuizServiceInterface qService = null;
	
	@Test
	public void contextLoads() {
		Assert.assertNull(null);
	}

	
	//----------------------------------------------------------------------------
	@Test
	public void testGetCourses() {
		
		List<Course> c = null;
		
		try {
			c = cService.getAvailableCourses();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(Course course: c) 
			System.out.println(course.getId() + " " + course.getFullname());
			
		Assert.assertNotNull(c);
		
	}
	
	
	//----------------------------------------------------------------------------
	@Test
	public void testGetStudentsCourses() {
		
		List<Student> c = null;
		
		try {
			c = sService.getStudentsInCourse("18");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("-----------------------------------");
		for(Student student: c) 
			System.out.println(student.getId() + " " + student.getFullname() + " " + student.getEmail());
			
		Assert.assertNotNull(c);
		
	}
	
	//----------------------------------------------------------------------------
	@Test
	public void testQuizCourses() {
		
		Quizzes c = null;
		
		try {
			c = qService.getQuizInCourse("17");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("-----------------------------------");
		for(Quiz quiz: c.getQuizzes()) 
			System.out.println(quiz.getId() + " " + quiz.getName());
			
		Assert.assertNotNull(c);
		
	}
	
	@Test
	public void testUnenrollStudent() {
		
		Student s = new Student();
		boolean answer = false;
		
		s = sService.getStudentInfoByUsername("h.contreras");
		s.setCourseid("17");
				
		answer = sService.unenrollStudent(s);
		
		Assert.assertTrue(answer);
	}
	
	//----------------------------------------------------------------------------
	@Test
	public void testEnrollStudent() {
		
		Student s = new Student();
		boolean answer = false;
		
		s.setUsername("h.contreras");
		s.setEmail("hermes.contreras@bwk.cl");
		s.setFirstname("Hermes");
		s.setLastname("Contreras");
		s.setCourseid("17");
		s.setPassword("P.p123456");
		s.setRoleid("5");  //student role
		try {
			answer = sService.enrollStudent(s);
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		Assert.assertTrue(answer);
	}

		

	
}
