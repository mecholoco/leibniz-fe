package com.bwk.leibniz.back.core.exception;

public class DataWrongException extends Exception {
	
	private static final long serialVersionUID = -5357468817355269923L;
	private String message = "";
	
	public DataWrongException(String message) {
		this.message = message;
	}
	
	public String toString(){ 
		return ("DataWrongException : " + this.message) ;
	}
}