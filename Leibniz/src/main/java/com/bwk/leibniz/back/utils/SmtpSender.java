package com.bwk.leibniz.back.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bwk.leibniz.front.configuration.ApplicationProperties;

@Component("SmtpSender")
public class SmtpSender { 		
	
	@Autowired	
	@Qualifier("ApplicationProperties")	
	private ApplicationProperties appc;
	
	
	public void sendHtmlEmail(String toAddress, String subject, String message, String fileName) throws MessagingException 
    {
 	
        Properties properties = new Properties();                
        properties.setProperty("mail.smtp.host", appc.getSmtp().getUri());
        properties.setProperty("mail.smtp.port", appc.getSmtp().getPort());
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
    
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(appc.getSmtp().getUser(), appc.getSmtp().getPassword());
            }
        };
        Session session = Session.getInstance(properties, auth);        
 
        try {
	        Message msg = new MimeMessage(session);	 
	        msg.setFrom(new InternetAddress(appc.getSmtp().getUser()));   //account from google currently      	         
	        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
	        msg.setSubject(subject);
	        msg.setSentDate(new Date());   
	        msg.setContent(message, "text/html");	

	        // Set the email msg text.
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(message, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            
	        if(!fileName.equals("")) {
	        	
	        	// Create the message part
	            BodyPart messageAttchPart = new MimeBodyPart();	          	           
	            DataSource source = new FileDataSource(fileName);
	            messageAttchPart.setDataHandler(new DataHandler(source));	            
	            
	            Path p = Paths.get(fileName);
	            String file = p.getFileName().toString();
	            
	            messageAttchPart.setFileName(file);
	            multipart.addBodyPart(messageAttchPart);

	            msg.setContent(multipart);
	        }	        
	        
	        Transport.send(msg);
        }
        catch(MessagingException mex) {
        	      	 
        }
         
    }


	
   
}
