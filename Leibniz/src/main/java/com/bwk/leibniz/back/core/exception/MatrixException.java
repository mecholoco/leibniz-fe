package com.bwk.leibniz.back.core.exception;

public class MatrixException extends Exception {
	
	private static final long serialVersionUID = 1174803051776806847L;
	private String message = "";
	
	public MatrixException(String message) {
		this.message = message;
	}
	
	public String toString(){ 
		return ("MatrixException : " + this.message) ;
	}
}
