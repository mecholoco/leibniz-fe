package com.bwk.leibniz.back.utils;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.Base64;

import org.apache.commons.lang3.ArrayUtils;

public class HBase64 {
	
	
	//---------------------------------------------------------------
	public static String encodeLocation(double[] doubleArray) {
		return Base64.getEncoder().encodeToString(doubleToByteArray(doubleArray));
	}
	
	
	//---------------------------------------------------------------
	public static String encodeLocation(double[][] doubleArray) {
		return Base64.getEncoder().encodeToString(doubleToByteArray(doubleArray));
	}
	
	//--------------------------------------------------------------- 
	public static double[] decodeLocation(String base64Encoded) {
		return byteToDoubleArray(Base64.getDecoder().decode(base64Encoded));
	}
	
		
	//---------------------------------------------------------------
	 private static byte[] doubleToByteArray(double[][] doubleArray) {
	 double[] simpleArray = null;

		 
		 for(int i = 0; i < doubleArray.length; i++) 
			 simpleArray =  ArrayUtils.addAll(simpleArray, doubleArray[i]);

		 
	     return doubleToByteArray(simpleArray);
	 }
	 
	 //---------------------------------------------------------------
	 private static byte[] doubleToByteArray(double[] doubleArray) {
     ByteBuffer buf = ByteBuffer.allocate(Double.SIZE / Byte.SIZE * doubleArray.length);
        buf.asDoubleBuffer().put(doubleArray);
        return buf.array();
	 }
		 
	//--------------------------------------------------------------- 
	private static double[] byteToDoubleArray(byte[] bytes) {
		        DoubleBuffer buf = ByteBuffer.wrap(bytes).asDoubleBuffer();
		        double[] doubleArray = new double[buf.limit()];
		        buf.get(doubleArray);
		        return doubleArray;
	}
	
	
		 
}
