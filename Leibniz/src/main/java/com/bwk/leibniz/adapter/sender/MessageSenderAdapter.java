package com.bwk.leibniz.adapter.sender;

import com.bwk.leibniz.front.entity.web.Student;

public interface MessageSenderAdapter {
	
	public boolean send(Student student, String subject, String message);
	public String createMessage(String requestURL, String companyName, Student student);
	
}
