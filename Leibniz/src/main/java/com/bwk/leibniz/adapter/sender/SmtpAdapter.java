package com.bwk.leibniz.adapter.sender;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bwk.leibniz.back.utils.SmtpSender;
import com.bwk.leibniz.front.entity.web.Student;

@Component("SmtpAdapter")
public class SmtpAdapter implements MessageSenderAdapter {

	@Autowired	
	@Qualifier("SmtpSender")	
	private SmtpSender sender;
		
	@Override
	public String createMessage(String requestURL, String companyName, Student student) {
		StringBuilder body = new StringBuilder();
		body.append("<html>" + "<head>"
				+ " <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">"
				+ "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\"> "
				+ "<style> body{font-family: 'Roboto', sans-serif;color:#000000;text-align: justify; font-size:12pt;}h3{color: #FC9610;}  .btn{ color:#ffffff; background:#FC9610; padding : 10px; text-decoration:none;  border-radius: 10px;} .btn:hover{  background:#1e79da; } </style>"
				+ "</head>" + "<body>" + "<h3>Bienvendio, "+ student.getFirstname() + " " + student.getLastname() + "</h3>"
				+ "<p>Te damos la bienvienida desde " + companyName + ".</p> <p>Su postulación sigue avanzando y le solicitamos haga click en el botón para realizar una prueba de conocimientos técnicos.</p>" 
				+ "<p>su nombre de usuario es: " + student.getUsername() + "</p>"
				+ "<p>su clave es: " + student.getPassword() + "</p>"
				+ "<a class=\"btn\" href=\"" + requestURL + "\">Activar Cuenta</a><br/>"
				+ "<br/><p>Buena Suerte!,<br/>"
				+ "</body>" + "</html>");

		return body.toString();
	}
	
	@Override
	public boolean send(Student student, String subject, String message) {		
		
		try {			
			sender.sendHtmlEmail(student.getEmail(), subject, message, "");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
				
	}
}
