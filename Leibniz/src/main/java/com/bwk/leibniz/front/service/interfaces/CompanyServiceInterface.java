package com.bwk.leibniz.front.service.interfaces;

import com.bwk.leibniz.front.entity.Company;

public interface CompanyServiceInterface {

	public abstract Company getCompany(String idCompany);
	public abstract Company addCompany(Company cmpy);	
	public abstract  Company getCompanyByMail(String mail); 	
	void UpdateValidatedCompany(String idComapny);
	public abstract boolean valMailRegister(String mail);
	
}
