package com.bwk.leibniz.front.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.bwk.leibniz.front.entity.Participant;
import com.bwk.leibniz.front.repository.ParticipantRepository;
import com.bwk.leibniz.front.service.interfaces.ParticipantServiceInterface;

@Service("ParticipantService")
public class ParticipantService implements ParticipantServiceInterface {

	@Autowired
	@Qualifier("ParticipantRepository")
	private ParticipantRepository ParticipantRepository;

	@Override
	public Participant addParticipant(Participant prtpant) {

		ParticipantRepository.save(prtpant);
		return prtpant;
	}

	public List<Participant> ListParticipantByStudy(String idStudy) {

		return ParticipantRepository.findByIdStudy(idStudy);

	}

	public void deleteParticipantsByIdStudy(String idStudy) {

		List<Participant> LParticipant = new ArrayList<>();
		LParticipant = ListParticipantByStudy(idStudy);

		if (LParticipant.size() > 0) {

			ParticipantRepository.delete(LParticipant);

		}

	}

	public Participant getParticipant(String idParticipant) {

		return ParticipantRepository.findByIdParticipant(idParticipant);
	}
	
	public Participant validarRut(String idRut) {
		
		return ParticipantRepository.findByParticipantRut(idRut);
	}

}
