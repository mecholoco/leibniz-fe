package com.bwk.leibniz.front.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Participant")
public class Participant {

	@Id 
	@Column(name = "idParticipant")
	private String idParticipant;
	
	@Column(name = "idCompany")
	private String idCompany;
	
	@Column(name = "idStudy")
	private String idStudy;	
	
	@Column(name = "idBusinessUnit")
	private int idBusinessUnit;
	
	@Column(name = "participantMail")
	private String participantMail;
	
	@Column(name = "participantName")
	private String participantName;
	
	@Column(name = "hasAnswered")
	private int hasAnswered;
	
	@Column(name = "participantRut")
	private String participantRut;

	public String getIdParticipant() {
		return idParticipant;
	}

	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}

	public String getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(String idCompany) {
		this.idCompany = idCompany;
	}

	public String getIdStudy() {
		return idStudy;
	}

	public void setIdStudy(String idStudy) {
		this.idStudy = idStudy;
	}

	public int getIdBusinessUnit() {
		return idBusinessUnit;
	}

	public void setIdBusinessUnit(int idBusinessUnit) {
		this.idBusinessUnit = idBusinessUnit;
	}

	public String getParticipantMail() {
		return participantMail;
	}

	public void setParticipantMail(String participantMail) {
		this.participantMail = participantMail;
	}

	public String getParticipantName() {
		return participantName;
	}

	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}

	public int getHasAnswered() {
		return hasAnswered;
	}

	public void setHasAnswered(int hasAnswered) {
		this.hasAnswered = hasAnswered;
	}

	public String getParticipantRut() {
		return participantRut;
	}

	public void setParticipantRut(String participantRut) {
		this.participantRut = participantRut;
	}

	public Participant(String idParticipant, String idCompany, String idStudy, int idBusinessUnit,
			String participantMail, String participantName, int hasAnswered, String participantRut) {
		super();
		this.idParticipant = idParticipant;
		this.idCompany = idCompany;
		this.idStudy = idStudy;
		this.idBusinessUnit = idBusinessUnit;
		this.participantMail = participantMail;
		this.participantName = participantName;
		this.hasAnswered = hasAnswered;
		this.participantRut = participantRut;
	}

	public Participant() {
		
	}

	

}
