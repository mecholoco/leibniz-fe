package com.bwk.leibniz.front.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "User")
public class User {
	
		
	@Id	
	@Size(min=2, max=30)
	@Column(name = "userName")
	private String userName;
		
	
	@Size(min=2, max=100)
	@Column(name = "password")
	private String password;
	
	@NotNull
	@Column(name = "idCompany")
	private String idCompany;
	
	@Column(name = "role")
	private String role;
	
	@Size(min=2, max=100)
	@Column(name = "realName")
	private String realName;

	@Column(name = "enabled")
	private int enabled;
	
	public User(String userName, String password, String idCompany, String role, String realName, int enabled) {
		super();		
		this.setUserName(userName);	
		this.setPassword(password);
		this.setIdCompany(idCompany);
		this.setRole(role);
		this.setRealName(realName);
		this.setEnabled(enabled);
	}

	public User() {
		
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	

	public String getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(String idCompany) {
		this.idCompany = idCompany;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRealName() {
		return realName;
	}

	private void setRealName(String realName) {
		this.realName = realName;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
	
	