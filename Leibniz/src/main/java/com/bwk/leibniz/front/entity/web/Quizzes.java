package com.bwk.leibniz.front.entity.web;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("quizzes")
public class Quizzes {
	@JsonProperty("quizzes")
	private List<Quiz> quizzes = null;

		
	@JsonProperty("quizzes")
	public List<Quiz> getQuizzes() {
		return quizzes;
	}

	@JsonProperty("quizzes")
	public void setQuizzes(List<Quiz> quizzes) {
		this.quizzes = quizzes;
	}
}
