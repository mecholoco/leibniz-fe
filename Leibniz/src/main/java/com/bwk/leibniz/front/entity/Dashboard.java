package com.bwk.leibniz.front.entity;

import java.util.List;

public class Dashboard {

	String percentStudy;
	int totSurveys;
	int MissingSurveys;
	int SurveysAnswered;
	int totAnswer;
	String RemainingDays;	

	public String getPercentStudy() {
		return percentStudy;
	}

	public void setPercentStudy(String percentStudy) {
		this.percentStudy = percentStudy;
	}

	public int getTotSurveys() {
		return totSurveys;
	}

	public void setTotSurveys(int totSurveys) {
		this.totSurveys = totSurveys;
	}

	public int getMissingSurveys() {
		return MissingSurveys;
	}

	public void setMissingSurveys(int missingSurveys) {
		MissingSurveys = missingSurveys;
	}

	public int getSurveysAnswered() {
		return SurveysAnswered;
	}

	public void setSurveysAnswered(int surveysAnswered) {
		SurveysAnswered = surveysAnswered;
	}

	public int getTotAnswer() {
		return totAnswer;
	}

	public void setTotAnswer(int totAnswer) {
		this.totAnswer = totAnswer;
	}

	public String getRemainingDays() {
		return RemainingDays;
	}

	public void setRemainingDays(String remainingDays) {
		RemainingDays = remainingDays;
	}

	public Dashboard() {
		super();

	}
}


	
	


