package com.bwk.leibniz.front.service.interfaces;

import org.springframework.web.multipart.MultipartFile;

public interface StorageServiceInterface {

	public String getFilePath();
	public boolean saveFile(MultipartFile file);
	
}
