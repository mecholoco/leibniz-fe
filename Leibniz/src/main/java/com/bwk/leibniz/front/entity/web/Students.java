package com.bwk.leibniz.front.entity.web;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonValue;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("users")
public class Students {
	
	@JsonProperty("users")
	private List<Student> students = null;	
	
	@JsonProperty("users")
	public List<Student> getStudents() {
		return students;
	}

	@JsonProperty("students")
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	@Override
	@JsonValue
    public String toString() {
        return "";
    }

}