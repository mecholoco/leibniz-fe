package com.bwk.leibniz.front.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bwk.leibniz.front.entity.Company;
import com.bwk.leibniz.front.repository.CompanyRepository;
import com.bwk.leibniz.front.service.interfaces.CompanyServiceInterface;

@Service("CompanyService")
public class CompanyService implements CompanyServiceInterface {

	@Autowired
	@Qualifier("CompanyRepository")
	private CompanyRepository cmpyRepository;
	
	public Company getCompany(String idCompany) {
		return cmpyRepository.findByIdCompany(idCompany);
	}

	public Company getCompanyByMail(String mail) {
		return cmpyRepository.findByContactMail(mail);
	}

	@Override
	public Company addCompany(Company cmpy) {
		// Encrypting password for contact company
		BCryptPasswordEncoder pweconder = new BCryptPasswordEncoder();
		String pw = "";
		pw = cmpy.getContactPwd();
		cmpy.setContactPwd(pweconder.encode(pw));
		cmpyRepository.save(cmpy);
		return cmpy;
	}

	@Override
	public void UpdateValidatedCompany(String idComapny) {

		Company existingCompany = cmpyRepository.findByIdCompany(idComapny);
		if (existingCompany != null) {
			existingCompany.setIsValidated(1);
			cmpyRepository.save(existingCompany);
		}
	}

	public boolean valMailRegister(String mail) {

		return cmpyRepository.existsByContactMail(mail);
	}

}
