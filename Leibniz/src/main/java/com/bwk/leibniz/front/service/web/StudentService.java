package com.bwk.leibniz.front.service.web;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bwk.leibniz.back.utils.EmailValidator;
import com.bwk.leibniz.front.configuration.ApplicationProperties;
import com.bwk.leibniz.front.entity.web.MoodleError;
import com.bwk.leibniz.front.entity.web.Student;
import com.bwk.leibniz.front.entity.web.Students;
import com.bwk.leibniz.front.service.web.interfaces.StudentServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("StudentService")
public class StudentService  implements StudentServiceInterface {

private RestTemplate restTemplate;

@Autowired
private ApplicationProperties app;

	
	//----------------------------------------------------------------------------
	public StudentService() {
		
		restTemplate = new RestTemplate();
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));
	}
	
	//----------------------------------------------------------------------------
		public Student getStudentInfoByid(String id) {
		String uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=core_user_get_users&wstoken=" + app.getMoodle().getCore_user_get_users_wstoken() + "&moodlewsrestformat=json";											
		String params = "&criteria[0][key]=id";
		params = params + "&criteria[0][value]=" + id;
		uri = uri + params;
		String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
        ObjectMapper mapper = new ObjectMapper();     
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);	 	       	       	                
        Students c = null;
        try {        	
        	c = mapper.readValue(jsonAnswer, Students.class);            	
        	
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mapper = null;        
		return c.getStudents().get(0);				
	}
		
	//----------------------------------------------------------------------------
	public Student getStudentInfoByUsername(String username) {
		String uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=core_user_get_users&wstoken=" + app.getMoodle().getCore_user_get_users_wstoken() + "&moodlewsrestformat=json";											
		String params = "&criteria[0][key]=username";
		params = params + "&criteria[0][value]=" + username;
		uri = uri + params;
		String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
        ObjectMapper mapper = new ObjectMapper();     
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);	 	       	       	                
        Students c = null;
        try {        	
        	c = mapper.readValue(jsonAnswer, Students.class);            	
        	
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mapper = null;        
		return c.getStudents().get(0);				
	}
	
	//----------------------------------------------------------------------------
	@Override
	public List<Student> getStudentsInCourse(String idCourse)  throws JsonGenerationException, JsonMappingException, IOException {								
		String uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=core_enrol_get_enrolled_users&wstoken=" + app.getMoodle().getCore_enrol_get_enrolled_users_wstoken() + "&moodlewsrestformat=json&courseid=" + idCourse;									
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));
			        
        String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
        ObjectMapper mapper = new ObjectMapper();     
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	 	       	       	                
        List<Student> c = null;
        try {        	
        	c = mapper.readValue(jsonAnswer, new TypeReference<List<Student>>(){} );            	
        	
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mapper = null;
        
		return c;
	}

	//----------------------------------------------------------------------------
	@Override
	public boolean enrollStudent(Student student) throws JsonGenerationException, JsonMappingException, IOException  {
		
		String uri = "";
		String params = "";
		String jsonAnswer = "";
		
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));			                	
		//create user in moodle
		uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=core_user_create_users&wstoken=" + app.getMoodle().getCore_user_create_users_wstoken() + "&moodlewsrestformat=json";
		params = "&users[0][username]=" + student.getUsername();
		params = params + "&users[0][password]=" + student.getPassword() ;
		params = params + "&users[0][firstname]=" + student.getFirstname();
		params = params + "&users[0][lastname]=" + student.getLastname();		
		params = params + "&users[0][email]=" + student.getEmail();
		params = params + "&users[0][auth]=manual";
		params = params + "&users[0][idnumber]=numberID";
		params = params + "&users[0][lang]=es";	
		uri = uri + params;
		jsonAnswer = restTemplate.getForObject(uri, String.class);
		ObjectMapper mapper = new ObjectMapper();     
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	 	       	       	                
        List<Student> c = null;
        try {        	
        	c = mapper.readValue(jsonAnswer, new TypeReference<List<Student>>(){} );            	
        	
        } catch (IOException e) {

        	if(jsonAnswer.length()>0) {        		
        		MoodleError error = mapper.readValue(jsonAnswer, MoodleError.class );  //user already exist mapp object to error
        		if(!error.getDebuginfo().toLowerCase().contains("username already exists"))
        			throw new RuntimeException(e);
        		else {
        			c = new ArrayList<Student>();  
        			c.add(getStudentInfoByUsername(student.getUsername()));
        		}
        	}
        	else
        		throw new RuntimeException(e);
        	
        }
        
        if(c != null) {		
        	student.setId( c.get(0).getId());
			// enroll in course
        	uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=enrol_manual_enrol_users&wstoken=" + app.getMoodle().getEnrol_manual_enrol_users_wstoken() + "&moodlewsrestformat=json";
			params = "&enrolments[0][roleid]=" + student.getRoleid();
			params = params + "&enrolments[0][userid]=" +	student.getId();
			params = params + "&enrolments[0][courseid]=" + student.getCourseid();
			uri = uri + params;
			jsonAnswer = restTemplate.getForObject(uri, String.class);
			
			return true;
        }	
        else        
        	return false;
		
	}

	//----------------------------------------------------------------------------
	@Override
	public boolean unenrollStudent(Student student) {
		String uri = "";
		String params = "";
		String jsonAnswer = "";
		
		uri = "http://" + app.getMoodle().getUri_base() + "/webservice/rest/server.php?wsfunction=enrol_manual_unenrol_users&wstoken=" + app.getMoodle().getEnrol_manual_unenrol_users_wstoken() + "&moodlewsrestformat=json";		
		params = "&enrolments[0][userid]=" + student.getId();
		params = params + "&enrolments[0][courseid]=" + student.getCourseid();
		uri = uri + params;
		jsonAnswer = restTemplate.getForObject(uri, String.class);
		
		if(jsonAnswer.equals("null"))
			return true;
		else
			return false;
	}
		
	//----------------------------------------------------------------------------
	private int validateBatchFile(String file) {
	BufferedReader br = null;
	EmailValidator validator = new EmailValidator();
	String line = null;	
		try {
			//validate structure
			br = new BufferedReader(new FileReader(file));		   
				while ((line = br.readLine()) != null) {
				  String[] values = line.split("\t");
				  	if(values.length != 3) {				  						  		
					  br.close();
					  return 1;
				  	}
				  	else if(values[0].length() == 0 || values[1].length() == 0 || !validator.validate(values[2])) { //valid name, last name and email
				  		br.close();
				  		return 1;
				  	}
				}
				br.close();		    		    				
			} catch (IOException e) {
					return 2;
			}
		return 0;
	}
	
	//----------------------------------------------------------------------------
	@Override
	public int enrollBatch(String file, String idRole, String idCourse) {
	BufferedReader br = null;
	String line = null;
	
		//validate structure
		int ret = validateBatchFile(file);
		if(ret != 0) return ret;
		try {			
			br = new BufferedReader(new FileReader(file));
		    while ((line = br.readLine()) != null) {
		      String[] values = line.split("\t");
		      Student s = new Student();
		      s.setFirstname(values[0]);
		      s.setLastname(values[1]);
		      s.setEmail(values[2]);
		      s.setUsername(s.getEmail());
		      s.setCourseid(idCourse);
		      s.setRoleid(idRole);
		      s.setPassword("P.p123456");
		      enrollStudent(s);
		    }
		    br.close();
		    
		} catch (FileNotFoundException e) {
			return 4;
		} catch (IOException e) {
			return 5;			
		}	    
	   
		return ret;
	}
		
}
