package com.bwk.leibniz.front.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bwk.leibniz.front.constant.ViewConstant;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("UserService")
	private UserDetailsService userService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		//auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());	
		auth.userDetailsService(userService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//.csrf().disable()   despues de http
		http			
		.authorizeRequests()
		.antMatchers("/css/*",
					 "/img/*",
					 "/js/*",
					 "/file/*",					 
					 "/home",
					 "/admin",					 					 					
					 "/login",
					 "/loginCheck")		
		.permitAll().anyRequest().authenticated()
		.antMatchers("/admin/applicant/add-batch").permitAll().anyRequest().permitAll()
		.antMatchers("/admin/applicant/**").permitAll().anyRequest().authenticated()
		.and()			
		.formLogin().loginPage("/login").loginProcessingUrl("/loginCheck")
		.usernameParameter("contactMail").passwordParameter("contactPwd")
		.defaultSuccessUrl("/home/evaluation").permitAll()
		.and()
		.logout().logoutUrl("/logout").logoutSuccessUrl("/login?logout").permitAll();
		
		
	}

}
