package com.bwk.leibniz.front.configuration;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter  {
	
	
	 //----------------------------------------------------------------------------
	 // Configuration i18n 		
	   @Bean
	   public LocaleResolver localeResolver() {
	       SessionLocaleResolver r = new SessionLocaleResolver();
	       r.setDefaultLocale(Locale.forLanguageTag("es-CL"));
		   //CookieLocaleResolver r = new CookieLocaleResolver();
	       //r.setDefaultLocale(Locale.forLanguageTag("es-CL"));
	       //r.setCookieName("localeInfo");
	       return r;
	   }	 
	
	 //----------------------------------------------------------------------------
	   @Bean
	   public LocaleChangeInterceptor localeChangeInterceptor() {
	       LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	       lci.setParamName("lang");
	       return lci;
	   }
	 
	 //----------------------------------------------------------------------------
	   @Override
	   public void addInterceptors(InterceptorRegistry registry) {
	       registry.addInterceptor(localeChangeInterceptor());
	   }
	  
	 //----------------------------------------------------------------------------
	   @Bean
	    public ResourceBundleMessageSource messageSource() {
	        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
	        source.setBasenames("i18n/messages");
	        source.setDefaultEncoding("UTF-8");
	        source.setUseCodeAsDefaultMessage(true);
	        return source;
	    }
	         
	   //----------------------------------------------------------------------------
}
