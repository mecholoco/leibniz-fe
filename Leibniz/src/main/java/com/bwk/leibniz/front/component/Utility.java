package com.bwk.leibniz.front.component;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


@Component
public class Utility {

	public String SendMail(String userName, String password, String msg, String to, String subject) {
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", true); // added this line
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.user", userName);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", true);

		System.out.println("declaracion");

		Session session = Session.getInstance(props, null);
		MimeMessage message = new MimeMessage(session);

		System.out.println("Port: " + session.getProperty("mail.smtp.port"));

		// Create the email addresses involved
		try {

			System.out.println("dentro de try");

			InternetAddress from = new InternetAddress(userName);
			message.setSubject(subject);
			message.setFrom(from);
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Create a multi-part to combine the parts
			Multipart multipart = new MimeMultipart("alternative");

			// Create your text message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Create the html part
			messageBodyPart = new MimeBodyPart();
			String htmlMessage = msg;
			messageBodyPart.setContent(htmlMessage, "text/html");

			// Add html part to multi part
			multipart.addBodyPart(messageBodyPart);

			// Associate multi-part with message
			message.setContent(multipart);

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", userName, password);
			System.out.println("Transport: " + transport.toString());

			transport.sendMessage(message, message.getAllRecipients());

		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "OK";
	}

	public String GenerateHashMD5(String keyString) {

		String md5 = null;
		try {
			MessageDigest mdEnc = MessageDigest.getInstance("MD5"); // Encryption algorithm
			mdEnc.update(keyString.getBytes(), 0, keyString.length());
			md5 = new BigInteger(1, mdEnc.digest()).toString(1); // Encrypted string
		} catch (Exception ex) {
			return null;
		}
		return md5.substring(0, 29);
	}

	public String GenerateMailActivation(String requestURL, String contactName, String companyName, String companyId) {

		StringBuilder body = new StringBuilder();

		body.append("<html>" + "<head>"
				+ " <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">"
				+ "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\"> "
				+ "<style> body{font-family: 'Roboto', sans-serif;color:#000000;text-align: justify; font-size:12pt;}h3{color: #FC9610;}  .btn{ color:#ffffff; background:#FC9610; padding : 10px; text-decoration:none;  border-radius: 10px;} .btn:hover{  background:#1e79da; } </style>"
				+ "</head>" + "<body>" + "<h3>Hola, "+ contactName+"</h3>"
				+ "<p>Te damos la bienvienida a Hypathia.</p> <p>Su inscripción se ha realizado correctamente, haga click en el botón para validar su cuenta de correo electrónico.</p>" 
				+ "<a class=\"btn\" href=\""+requestURL+"valEmail?Code=" + companyId  +"\">Activar Cuenta</a><br/>"
				+ "<br/><p>Atentamenta,<br/>"
				+ "el equipo de Hypathia.</p>"
				 + "</body>" + "</html>");

		return body.toString();
	}

	public String GenerateMailSurvey(String requestURL, String companyName, String idStudy, String participantId, String nameParticipant) {

		StringBuilder body = new StringBuilder();

		body.append("<html>" + "<head>"
				+ " <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">"
				+ "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\"> "
				+ "<style> body{font-family: 'Roboto', sans-serif;color:#000000;text-align: justify; font-size:12pt;}h3{color: #FC9610;}  .btn{ color:#ffffff; background:#FC9610; padding : 10px; text-decoration:none;  border-radius: 10px;} .btn:hover{  background:#1e79da; } </style>"
				+ "</head>" + "<body>" + "<h3>Bienvenido a la encuesta de compromiso laboral</h3>"
				+"<p>"+nameParticipant + ".</p>"
				+ "<p>Te invitamos a contestar la siguiente encuenta de compromiso laboral creada por <b>" + companyName +"</b>.<br/>"
					
				+ "<br/><br/><a class=\"btn\" href=\""+requestURL+"getSurvey?code=" + idStudy + "&part=" + participantId + "\">Contestar encuesta </a>"
				+ "<br/><br/><br/><p> Gracias  de antemano por su participación.</p>"
				+ "<p>Atentamenta,<br/>"
				+ "el equipo de Hypathia,</p>"
				+ "</body>" + "</html>");

		return body.toString();
	}

	public static Document getParser(String xml) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		File file = new File(xml);
		Document doc = builder.parse(file);
		return doc;
	}
	
	
	public String getBaseUrl (HttpServletRequest request) {
		
		String url =null;
		
		String scheme = request.getScheme();
        String serverName = request.getServerName();
        int portNumber = request.getServerPort();
        String contextPath = request.getContextPath();
    
        
        url = scheme +"://"+serverName +":"+Integer.toString(portNumber)+contextPath+"/";
		
		
		return url;
		
	}
	

}