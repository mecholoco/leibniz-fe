package com.bwk.leibniz.front.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bwk.leibniz.front.configuration.ApplicationProperties;
import com.bwk.leibniz.front.service.interfaces.StorageServiceInterface;

@Service("StorageService")
public class StorageService implements StorageServiceInterface {
	
	@Autowired
	private ApplicationProperties app;
	
	private String repositoryFolder = "";
	private String filePath = "";
	
	public StorageService() {
		
	}
	
			
	
	@Override
	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	@Override
	public boolean saveFile(MultipartFile file) {		 
		byte[] bytes;
		
		Path resourceDirectory = Paths.get("resources/upload");	
		repositoryFolder = app.getLocal_file_repository() + resourceDirectory.toString() + "/";
        if (!file.isEmpty()) {                    	
			try {
				bytes = file.getBytes();
				Path path = Paths.get(repositoryFolder + "/" + file.getOriginalFilename());
				Files.write(path, bytes);
			} catch (IOException e) {			
				e.printStackTrace();
				return false;
			}	        
        }
        
        setFilePath(repositoryFolder + "/" + file.getOriginalFilename());
		return true;		
	}

}
