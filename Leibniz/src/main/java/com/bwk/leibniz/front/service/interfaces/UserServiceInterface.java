package com.bwk.leibniz.front.service.interfaces;

import com.bwk.leibniz.front.entity.User;


public interface UserServiceInterface {

	public User findByUserName(String userName);
	public User getUserLogin();
	
}
