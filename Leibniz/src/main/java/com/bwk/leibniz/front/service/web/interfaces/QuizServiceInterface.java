package com.bwk.leibniz.front.service.web.interfaces;

import java.io.IOException;

import com.bwk.leibniz.front.entity.web.Quizzes;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface QuizServiceInterface {

	public Quizzes getQuizInCourse(String idCourse)  throws JsonGenerationException, JsonMappingException, IOException;
	
}
