package com.bwk.leibniz.front.repository;

import java.io.Serializable;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bwk.leibniz.front.entity.Participant;


@Repository("ParticipantRepository")
public interface ParticipantRepository extends JpaRepository<Participant, Serializable> { 

	public List<Participant> findByIdStudy(String IdStudy);		
	public Participant findByIdParticipant(String idParticipant);	
	public Participant findByParticipantRut(String idRut);	
	
}
