package com.bwk.leibniz.front.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bwk.leibniz.front.configuration.ApplicationProperties;
import com.bwk.leibniz.front.service.interfaces.StorageServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.StudentServiceInterface;

@RestController
public class RestUploadController {
	
	@Autowired
	@Qualifier("StorageService")
	private StorageServiceInterface  storageService;
	
	@Autowired
	@Qualifier("StudentService")
	private StudentServiceInterface  studentService;
	
	@Autowired
	private ApplicationProperties app;
	
    private final Logger logger = LoggerFactory.getLogger(RestUploadController.class);    
    
    public RestUploadController() {        	
    	
    }    
     
    // Single file upload        
    @PostMapping(value = "/admin/applicant/add-batch")    
    @ResponseBody
    public ResponseEntity<Object> uploadFile(@RequestParam("idCourse") String id,
    										 @RequestParam(value = "fileInput", required = true) MultipartFile uploadedFile) {        
        
        if (uploadedFile.isEmpty()) 
            return new ResponseEntity<Object>("please select a file!", HttpStatus.OK);        

        if(!storageService.saveFile(uploadedFile))
            return new ResponseEntity<Object>("Empty file/Can't save file in storage", HttpStatus.BAD_REQUEST);
        else {        	
        	int status = studentService.enrollBatch(storageService.getFilePath(), app.getMoodle().getStudent_role(), id);        
        	if(status == 0)
        		return new ResponseEntity<Object>("Successfully uploaded - " + uploadedFile.getOriginalFilename(), new HttpHeaders(), HttpStatus.OK);
        	else {
        		return new ResponseEntity<Object>("Error in batch enrolling, review file format.", HttpStatus.BAD_REQUEST);
        	}
        }	
    }
        
}