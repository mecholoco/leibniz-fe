package com.bwk.leibniz.front.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bwk.leibniz.front.entity.Company;
import com.bwk.leibniz.front.repository.UserRepository;
import com.bwk.leibniz.front.service.interfaces.CompanyServiceInterface;
import com.bwk.leibniz.front.service.interfaces.UserServiceInterface;

@Service("UserService")
public class UserService implements UserDetailsService, UserServiceInterface {

	@Autowired
	@Qualifier("CompanyService")
	private CompanyServiceInterface  cmpnyService;
	
	@Autowired
	@Qualifier("UserRepository")
	private UserRepository  uRepository;
	
	public UserService() {
		
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {				
		com.bwk.leibniz.front.entity.User user = findByUserName(username);	
		Company nwCompany = cmpnyService.getCompany(user.getIdCompany());				
		GrantedAuthority authority = new SimpleGrantedAuthority(Integer.toString(nwCompany.getIsPremium()));				
		User usr = new User(user.getUserName(), user.getPassword(), Arrays.asList(authority));		
		UserDetails usrDetails = (UserDetails) usr;
					
		return usrDetails;
	}

	@Override
	public com.bwk.leibniz.front.entity.User getUserLogin() {		
		User usrLogin = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		return uRepository.findByUserName(usrLogin.getUsername());				
	}

	@Override
	public com.bwk.leibniz.front.entity.User findByUserName(String userName) {
		return uRepository.findByUserName(userName);		
	}

	
}
