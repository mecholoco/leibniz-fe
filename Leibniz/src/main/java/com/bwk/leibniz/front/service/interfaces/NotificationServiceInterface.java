package com.bwk.leibniz.front.service.interfaces;

import com.bwk.leibniz.front.entity.web.Student;

public interface NotificationServiceInterface {

	public boolean send(Student student);
	
}
