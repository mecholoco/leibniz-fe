package com.bwk.leibniz.front.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Company")
public class Company {

	@Id
	@Column(name = "idCompany")
	private String idCompany;

	@Column(name = "companyName")
	private String companyName;

	@Column(name = "companyAddress")
	private String companyAddress;

	@Column(name = "contactName")
	private String contactName;

	@Column(name = "contactMail")
	private String contactMail;

	@Column(name = "contactPhone")
	private String contactPhone;

	@Column(name = "isValidated")
	private int isValidated;

	@Column(name = "idTypeAccount")
	private int idTypeAccount;

	@Column(name = "contactPwd")
	private String contactPwd;

	@Column(name = "isPremium")
	private int isPremium;

	public String getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(String idCompany) {
		this.idCompany = idCompany;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactMail() {
		return contactMail;
	}

	public void setContactMail(String contactMail) {
		this.contactMail = contactMail;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public int getIsValidate() {
		return isValidated;
	}

	public void setIsValidated(int isValidated) {
		this.isValidated = isValidated;
	}

	public int getIdTypeAccount() {
		return idTypeAccount;
	}

	public void setIdTypeAccount(int idTypeAccount) {
		this.idTypeAccount = idTypeAccount;
	}

	public String getContactPwd() {
		return contactPwd;
	}

	public void setContactPwd(String contactPwd) {
		this.contactPwd = contactPwd;
	}

	public int getIsPremium() {
		return isPremium;
	}

	public void setIsPremium(int isPremium) {
		this.isPremium = isPremium;
	}

	public Company(String idCompany, String companyName, String companyAddress, String contactName, String contactMail,
			String contactPhone, int isValidate, int idTypeAccount, String contactPwd, int isPremium) {
		super();
		this.idCompany = idCompany;
		this.companyName = companyName;
		this.companyAddress = companyAddress;
		this.contactName = contactName;
		this.contactMail = contactMail;
		this.contactPhone = contactPhone;
		this.isValidated = isValidate;
		this.idTypeAccount = idTypeAccount;
		this.contactPwd = contactPwd;
		this.isPremium = isPremium;
	}

	public Company() {
	}

}
