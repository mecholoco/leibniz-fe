package com.bwk.leibniz.front.service.web.interfaces;

import java.io.IOException;
import java.util.List;

import com.bwk.leibniz.front.entity.web.Course;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface CourseServiceInterface {

	public List<Course> getAvailableCourses() throws JsonGenerationException, JsonMappingException, IOException;
	public Course getCourseInfo(String id) throws JsonGenerationException, JsonMappingException, IOException;
	
}
