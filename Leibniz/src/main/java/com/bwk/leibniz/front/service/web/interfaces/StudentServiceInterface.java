package com.bwk.leibniz.front.service.web.interfaces;

import java.io.IOException;
import java.util.List;

import com.bwk.leibniz.front.entity.web.Student;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface StudentServiceInterface {

	public Student getStudentInfoByUsername(String username);
	public Student getStudentInfoByid(String id);
	public List<Student> getStudentsInCourse(String idCourse) throws JsonGenerationException, JsonMappingException, IOException;
	public boolean enrollStudent(Student student) throws JsonGenerationException, JsonMappingException, IOException;
	public boolean unenrollStudent(Student student);
	public int enrollBatch(String file, String idRole, String idCourse);
		
}
