package com.bwk.leibniz.front.controller;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bwk.leibniz.front.constant.ViewConstant;

@Controller

public class LoginController {

	@GetMapping("/login")
	public String showlogin(Model md, @RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout,
			@RequestParam(name = "subscription", required = false) String subscription) {

		md.addAttribute("error", error);
		md.addAttribute("logout", logout);
		md.addAttribute("subscription", subscription);

		return ViewConstant.PUBLIC_LOGIN;
	}

	@GetMapping("/loginsuccess")
	public String loginCheck() {
	
		return "redirect:/home/evaluation";
		
	}

}
