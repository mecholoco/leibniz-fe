package com.bwk.leibniz.front.entity.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonPropertyOrder({ "id", "fullname" })
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("course")
public class Course {
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("fullname")
	private String fullname;
			
	@Override
	@JsonValue
    public String toString() {
        return Integer.toString(getId()) + " " + getFullname();
    }

	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
