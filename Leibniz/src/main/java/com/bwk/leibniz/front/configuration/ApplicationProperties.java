package com.bwk.leibniz.front.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("ApplicationProperties")
@ConfigurationProperties("configuration") // prefix app, find app.* values
public class ApplicationProperties {

    private String company;
    private String local_file_repository;
    private Smtp smtp = new Smtp();
    private Moodle moodle = new Moodle();
   
    public static class Smtp {
    	private String uri;
    	private String port;
    	private String user; 
    	private String password;
        private String timeout;
        private String outputFolder;
		public String getUri() {
			return uri;
		}
		public void setUri(String uri) {
			this.uri = uri;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getTimeout() {
			return timeout;
		}
		public void setTimeout(String timeout) {
			this.timeout = timeout;
		}
		public String getOutputFolder() {
			return outputFolder;
		}
		public void setOutputFolder(String outputFolder) {
			this.outputFolder = outputFolder;
		}

    }
    
    public static class Moodle {
    	private String student_role;               
    	private String uri_base;        
    	private String core_user_get_users_wstoken;
    	private String core_user_create_users_wstoken;
    	private String core_enrol_get_enrolled_users_wstoken;
    	private String enrol_manual_unenrol_users_wstoken;
    	private String enrol_manual_enrol_users_wstoken;
    		
		public String getStudent_role() {
			return student_role;
		}
		public void setStudent_role(String student_role) {
			this.student_role = student_role;
		}
		public String getUri_base() {
			return uri_base;
		}
		public void setUri_base(String uri_base) {
			this.uri_base = uri_base;
		}
		public String getCore_user_get_users_wstoken() {
			return core_user_get_users_wstoken;
		}
		public void setCore_user_get_users_wstoken(String core_user_get_users_wstoken) {
			this.core_user_get_users_wstoken = core_user_get_users_wstoken;
		}
		public String getCore_user_create_users_wstoken() {
			return core_user_create_users_wstoken;
		}
		public void setCore_user_create_users_wstoken(String core_user_create_users_wstoken) {
			this.core_user_create_users_wstoken = core_user_create_users_wstoken;
		}
		public String getCore_enrol_get_enrolled_users_wstoken() {
			return core_enrol_get_enrolled_users_wstoken;
		}
		public void setCore_enrol_get_enrolled_users_wstoken(String core_enrol_get_enrolled_users_wstoken) {
			this.core_enrol_get_enrolled_users_wstoken = core_enrol_get_enrolled_users_wstoken;
		}
		public String getEnrol_manual_unenrol_users_wstoken() {
			return enrol_manual_unenrol_users_wstoken;
		}
		public void setEnrol_manual_unenrol_users_wstoken(String enrol_manual_unenrol_users_wstoken) {
			this.enrol_manual_unenrol_users_wstoken = enrol_manual_unenrol_users_wstoken;
		}
		public String getEnrol_manual_enrol_users_wstoken() {
			return enrol_manual_enrol_users_wstoken;
		}
		public void setEnrol_manual_enrol_users_wstoken(String enrol_manual_enrol_users_wstoken) {
			this.enrol_manual_enrol_users_wstoken = enrol_manual_enrol_users_wstoken;
		}   
    }

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLocal_file_repository() {
		return local_file_repository;
	}

	public void setLocal_file_repository(String local_file_repository) {
		this.local_file_repository = local_file_repository;
	}

	public Smtp getSmtp() {
		return smtp;
	}

	public void setSmtp(Smtp smtp) {
		this.smtp = smtp;
	}

	public Moodle getMoodle() {
		return moodle;
	}

	public void setMoodle(Moodle moodle) {
		this.moodle = moodle;
	}
    
}
