package com.bwk.leibniz.front.service.web;

import java.io.IOException;

import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bwk.leibniz.front.entity.web.Quizzes;
import com.bwk.leibniz.front.service.web.interfaces.QuizServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("QuizService")
public class QuizService implements QuizServiceInterface {

	private RestTemplate restTemplate;
	
		//----------------------------------------------------------------------------
		public QuizService() {
			
			restTemplate = new RestTemplate();
		}
		
		//----------------------------------------------------------------------------
		public Quizzes getQuizInCourse(String idCourse)  throws JsonGenerationException, JsonMappingException, IOException {								
			String uri = "http://34.205.74.45:80/webservice/rest/server.php?wsfunction=mod_quiz_get_quizzes_by_courses&wstoken=959f10466349504f13683f443fbf6c15&moodlewsrestformat=json&courseids[0]=" + idCourse;									
			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));
				        
	        String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
	        ObjectMapper mapper = new ObjectMapper();     
	        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	 	       	       	                
	        Quizzes c = null;
            try {
            	c = mapper.readValue(jsonAnswer, Quizzes.class );            	
            	
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
	        mapper = null;
	        
			return c;
		}
			
		
}
