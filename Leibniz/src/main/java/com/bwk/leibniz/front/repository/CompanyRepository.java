package com.bwk.leibniz.front.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bwk.leibniz.front.entity.Company;


@Repository("CompanyRepository")
public interface CompanyRepository extends JpaRepository<Company, Serializable> {
	
	public Company findByContactMail(String mail);
	public Company findByIdCompany(String idCompany);
	public Company findOneByContactMail(String contactMail);	
	public boolean existsByContactMail(String contactMail);
	public boolean existsByIdCompanyAndIsValidated(String idCompany, int validate);
	
}
