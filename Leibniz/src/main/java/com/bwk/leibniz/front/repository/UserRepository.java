package com.bwk.leibniz.front.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bwk.leibniz.front.entity.User;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<User, Serializable> {

	public User findByUserName(String usrName);
	public List<User> findByIdCompany(String idCompany);

}
