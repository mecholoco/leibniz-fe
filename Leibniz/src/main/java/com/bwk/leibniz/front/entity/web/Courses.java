package com.bwk.leibniz.front.entity.web;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonValue;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("courses")
public class Courses {
	
	@JsonProperty("courses")
	private List<Course> courses = null;

	@JsonProperty("courses")
	public List<Course> getCourses() {
	return courses;
	}

	@JsonProperty("courses")
	public void setData(List<Course> courses) {
	this.courses = courses;
	}
	
	@Override
	@JsonValue
    public String toString() {
        return "";
    }

}
