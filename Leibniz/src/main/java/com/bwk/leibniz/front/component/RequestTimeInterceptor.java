package com.bwk.leibniz.front.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component("RequestTimeInterceptor")
public class RequestTimeInterceptor extends HandlerInterceptorAdapter {

	private static final Log lOGS = LogFactory.getLog(RequestTimeInterceptor.class);

	// Primera pasada antes de llamar al controlador
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		request.setAttribute("startTime", System.currentTimeMillis());

		return true;
	}

	// Pasada desde el controlador hacia la vista
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long startTime = (long) request.getAttribute("startTime");
		lOGS.info("[REQUEST URL] :'" + request.getRequestURL().toString() + "'[TOTAL TIME] :'"+ (System.currentTimeMillis() - startTime) + "'ms'");

	}

}
