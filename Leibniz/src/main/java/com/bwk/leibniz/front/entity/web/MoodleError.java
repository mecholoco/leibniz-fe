package com.bwk.leibniz.front.entity.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonPropertyOrder({ "exception", "errorcode", "message", "debuginfo" })
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("error")
public class MoodleError {
	
	@JsonProperty("exception")
	private String exception;
	
	@JsonProperty("errorcode")
	private String errorcode;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("debuginfo")
	private String debuginfo;

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDebuginfo() {
		return debuginfo;
	}

	public void setDebuginfo(String debuginfo) {
		this.debuginfo = debuginfo;
	}
}
