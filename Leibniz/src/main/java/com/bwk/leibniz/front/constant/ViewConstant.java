package com.bwk.leibniz.front.constant;

public class ViewConstant {

// PUBLIC HTML VIEWS 	
	public static final String PUBLIC_REGISTER = "register";
	public static final String PUBLIC_LOGIN = "login";
	public static final String PUBLIC_VAL_EMAIL = "emailmsg";
	public static final String PUBLIC_MSG = "message";	

// PRIVATE HTML VIEWS
	public static final String HOME_EVAL_PANEL = "homePanel";
	public static final String ADMIN_APPLICANTS = "adminapplicants";
	public static final String PRIVATE_DASHBOARD ="dashboard";
	
//OTHER CONSTANTS	
	public static final String USER_MAIL = "blauwekarper@gmail.com";
	public static final String PASS_MAIL = "blauwe.123_";
		

	
}

