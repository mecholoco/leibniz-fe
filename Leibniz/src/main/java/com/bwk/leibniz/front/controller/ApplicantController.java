package com.bwk.leibniz.front.controller;

import java.io.IOException;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bwk.leibniz.front.configuration.ApplicationProperties;
import com.bwk.leibniz.front.constant.ViewConstant;
import com.bwk.leibniz.front.entity.Company;
import com.bwk.leibniz.front.entity.User;
import com.bwk.leibniz.front.entity.web.Course;
import com.bwk.leibniz.front.entity.web.Student;
import com.bwk.leibniz.front.service.interfaces.CompanyServiceInterface;
import com.bwk.leibniz.front.service.interfaces.NotificationServiceInterface;
import com.bwk.leibniz.front.service.interfaces.UserServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.CourseServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.StudentServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("/")
public class ApplicantController {

	@Autowired
	@Qualifier("CompanyService")
	private CompanyServiceInterface cmpnyService;

	@Autowired
	@Qualifier("UserService")
	private UserServiceInterface usrService;
	
	@Autowired
	@Qualifier("StudentService")
	private StudentServiceInterface studentService;
	
	@Autowired
	@Qualifier("CourseService")
	private CourseServiceInterface courseService;
	
	@Autowired
	@Qualifier("NotificationService")
	private NotificationServiceInterface notificationService;
	
	@Autowired
	@Qualifier("ApplicationProperties")
	private ApplicationProperties app;
			
	//----------------------------------------------------------------------------
	@RequestMapping(value = "/admin/applicant" ,  method = { RequestMethod.GET, RequestMethod.POST })
	public String showApplicants(Model md, HttpServletRequest request,
								@RequestParam(name = "idCourse", required = false) String id) throws JsonGenerationException, JsonMappingException, IOException { 
		
		User user = usrService.getUserLogin();				
		Company nwCompany = cmpnyService.getCompany(user.getIdCompany());		
		Course course = courseService.getCourseInfo(id);
					
		md.addAttribute("ListApplicant", studentService.getStudentsInCourse(id));
		md.addAttribute("course", course);
		md.addAttribute("user", user);
		md.addAttribute("company", nwCompany);
		md.addAttribute("RegisterAddStudent", new Student());
		return ViewConstant.ADMIN_APPLICANTS;

	}
	
	//----------------------------------------------------------------------------
	@RequestMapping(value = "/admin/applicant/add" ,  method = { RequestMethod.GET, RequestMethod.POST })
	public String addApplicants(@ModelAttribute(name = "RegisterAddStudent") Student student,  Model md,
								HttpServletRequest request,	@RequestParam(name = "idCourse", required = true) String id) throws JsonGenerationException, JsonMappingException, IOException { 
		boolean result = false;
		
		//complete lacking 
		student.setCourseid(id);
		student.setRoleid(app.getMoodle().getStudent_role());
		student.setPassword("P.p123456");
		student.setEmail(student.getEmail().toLowerCase());
		student.setUsername(student.getEmail());
		try {
			result = studentService.enrollStudent(student);
		}
		catch(RuntimeException ex) {
			md.addAttribute("description", "Can't add user");
		}
							
		md.addAttribute("result", result);		
		return "forward:/admin/applicant";		

	}
	
	
	//----------------------------------------------------------------------------
		@RequestMapping(value = "/admin/applicant/delete" ,  method = { RequestMethod.GET, RequestMethod.POST })
		public String delApplicants(Model md,HttpServletRequest request,	
									@RequestParam(name = "idStudent", required = true) String id,
									@RequestParam(name = "idCourse", required = true) String idCourse) throws JsonGenerationException, JsonMappingException, IOException { 
					
			Student student = new Student();
			student.setId(id);
			student.setCourseid(idCourse);
			studentService.unenrollStudent(student);
			boolean result = studentService.unenrollStudent(student);
								
			md.addAttribute("result", result);
			return "forward:/admin/applicant";		

		}				
		
		//----------------------------------------------------------------------------
		@RequestMapping(value = "/admin/applicant/send-notification" ,  method = { RequestMethod.GET, RequestMethod.POST })
		public String sendNotificationApplicant(Model md,HttpServletRequest request,	
									@RequestParam(name = "idStudent", required = true) String id,
									@RequestParam(name = "username", required = true) String username,
									@RequestParam(name = "email", required = true) String email,
									@RequestParam(name = "firstname", required = true) String firstName,
									@RequestParam(name = "lastname", required = true) String lastName,
									@RequestParam(name = "idCourse", required = true) String idCourse) throws JsonGenerationException, JsonMappingException, IOException { 
					
			Student student = new Student();
			student.setId(id);			
			student.setCourseid(idCourse);
			student.setUsername(username);
			student.setFirstname(firstName);				
			student.setLastname(lastName);
			student.setEmail(email);
			student.setPassword("P.p123456");
						
			boolean result = notificationService.send(student);
								
			md.addAttribute("result", result);
			return "forward:/admin/applicant";		

		}

}
