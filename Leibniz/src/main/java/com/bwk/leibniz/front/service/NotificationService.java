package com.bwk.leibniz.front.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bwk.leibniz.adapter.sender.MessageSenderAdapter;
import com.bwk.leibniz.front.configuration.ApplicationProperties;
import com.bwk.leibniz.front.entity.web.Student;
import com.bwk.leibniz.front.service.interfaces.NotificationServiceInterface;

@Service("NotificationService")
public class NotificationService implements NotificationServiceInterface {
				
	@Autowired
	private ApplicationProperties app;
	
	@Autowired
	@Qualifier("SmtpAdapter")
	private MessageSenderAdapter adapter;
	
	public NotificationService() {		
	}
	
		
	@Override
	public boolean send(Student student) {
		
		String subject = "Evaluación técnica en linea.";				
		String message = adapter.createMessage(app.getMoodle().getUri_base(), app.getCompany(), student);
		if(adapter.send(student, subject, message)) 
			return true;
		else
			return false;
		
	}
	
	 

}
