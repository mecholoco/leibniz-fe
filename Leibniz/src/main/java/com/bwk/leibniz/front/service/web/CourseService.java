package com.bwk.leibniz.front.service.web;

import java.io.IOException;
import java.util.List;

import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bwk.leibniz.front.entity.web.Course;
import com.bwk.leibniz.front.entity.web.Courses;
import com.bwk.leibniz.front.service.web.interfaces.CourseServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("CourseService")
public class CourseService implements CourseServiceInterface {

	private RestTemplate restTemplate;
	
	
	//----------------------------------------------------------------------------
	public CourseService() {
		
		restTemplate = new RestTemplate();
	}
	
	//----------------------------------------------------------------------------
		public List<Course> getAvailableCourses() throws JsonGenerationException, JsonMappingException, IOException {								
			String uri = "http://34.205.74.45:80/webservice/rest/server.php?wsfunction=core_course_get_courses_by_field&wstoken=a6f261b67e0217adc039b0710960d29d&moodlewsrestformat=json";			
			String params = "&field=category&value=1";
			
			uri = uri + params;
			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));
				       
	        String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
	        ObjectMapper mapper = new ObjectMapper();     
	        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	 	       	       	                    
            Courses c = null;
            try {
            	c = mapper.readValue(jsonAnswer, Courses.class );            	
            	
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            mapper = null;
            
			return c.getCourses();
		}
		
	//----------------------------------------------------------------------------
			public Course getCourseInfo(String id) throws JsonGenerationException, JsonMappingException, IOException {								
				String uri = "http://34.205.74.45:80/webservice/rest/server.php?wsfunction=core_course_get_courses_by_field&wstoken=a6f261b67e0217adc039b0710960d29d&moodlewsrestformat=json";
				String params = "&field=id&value=" + id;
				
				uri = uri + params;
				restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("wsuser", "Blauwe.123!"));
					        
		        String jsonAnswer = restTemplate.getForObject(uri, String.class);	        	        
		        ObjectMapper mapper = new ObjectMapper();     
		        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	 	       	       	                    
	            Courses c = null;
	            try {
	            	c = mapper.readValue(jsonAnswer, Courses.class );            	
	            	
	            } catch (IOException e) {
	                throw new RuntimeException(e);
	            }
	            mapper = null;
	            
				return c.getCourses().get(0);
			}
		
		
}
