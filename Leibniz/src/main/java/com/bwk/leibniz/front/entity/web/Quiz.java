package com.bwk.leibniz.front.entity.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonPropertyOrder({ "id", "course", "coursemodule", "name" })
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("quiz")
public class Quiz {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("course")
	private String course;
	
	@JsonProperty("coursemodule")
	private String coursemodule;
	
	@JsonProperty("name")
	private String name;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getCoursemodule() {
		return coursemodule;
	}
	public void setCoursemodule(String coursemodule) {
		this.coursemodule = coursemodule;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
