package com.bwk.leibniz.front.controller;

import java.io.IOException;
import java.util.List;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bwk.leibniz.front.constant.ViewConstant;
import com.bwk.leibniz.front.entity.Company;
import com.bwk.leibniz.front.entity.User;
import com.bwk.leibniz.front.entity.web.Course;
import com.bwk.leibniz.front.service.interfaces.CompanyServiceInterface;
import com.bwk.leibniz.front.service.interfaces.UserServiceInterface;
import com.bwk.leibniz.front.service.web.interfaces.CourseServiceInterface;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
	@Qualifier("CompanyService")
	private CompanyServiceInterface cmpnyService;

	@Autowired
	@Qualifier("UserService")
	private UserServiceInterface usrService;
	
	@Autowired
	@Qualifier("CourseService")
	private CourseServiceInterface crService;
			
	@RequestMapping(value = "/home/evaluation" ,  method = { RequestMethod.GET, RequestMethod.POST })
	public String showAdmin(Model nwModel, HttpServletRequest request) throws JsonGenerationException, JsonMappingException, IOException { 
		
		User user = usrService.getUserLogin();				
		Company nwCompany = cmpnyService.getCompany(user.getIdCompany());
					
		List<Course> listCourses = crService.getAvailableCourses(); 		               		
		nwModel.addAttribute("ListCourses", listCourses);
		nwModel.addAttribute("user", user);
		nwModel.addAttribute("company", nwCompany);
		return ViewConstant.HOME_EVAL_PANEL;

	}
	

}
