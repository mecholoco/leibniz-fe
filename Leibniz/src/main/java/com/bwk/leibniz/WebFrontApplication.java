package com.bwk.leibniz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(value = {"com.bwk.leibniz.adapter.sender", "com.bwk.leibniz.back.utils", "com.bwk.leibniz.front.configuration", "com.bwk.leibniz.front.service", "com.bwk.leibniz.front.service.web", "com.bwk.leibniz.front.controller", "com.bwk.leibniz.front.controller.rest"})
@EntityScan(basePackages = {"com.bwk.leibniz.front.entity", "com.bwk.leibniz.front.entity.web"})
@EnableJpaRepositories({"com.bwk.leibniz.front.repository"})
public class WebFrontApplication extends SpringBootServletInitializer {
			 	
	public static void main(String[] args) {
		SpringApplication.run(WebFrontApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(WebFrontApplication.class);
	}
	
		
}